<%@ page import="org.bmlaurus.home.Prop"%>
<!--
Theme Name: iDempiere Theme
Version: 2.0
Description: iDempiere Theme
Author: BMLaurus
Author URI: http://www.bmlaurus.com
-->
<html>
<%
	Prop.load();
%>
<head>
<meta name="viewport"
	content="width=device-width, minimum-scale=1.0, maximum-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="<%=Prop.loadCss()%>">
<script type="text/javascript" src="resources/jquery/jquery.js"></script>
<%

%>
<title><%=Prop.getProperty(Prop.TITLE)%></title>
</head>

<body>


	<section>
	<!-- <div class="bg"></div>
		<div class="bg2"></div>
		<div class="bg3"></div>
		<div class="bg4"></div>
		<div class="bg5"></div>
		<div class="bg6"></div>-->
		<div class="rectangle">

			<div id="main-login" class="main-login">
				<a href="<%=Prop.getProperty(Prop.WEBUI_LINK)%>" target="_self">
					<img name="img_webui" src="<%=Prop.getImage("img_webui.png")%>"
					border="0" />
				</a>
				<br>
				<a target="_blank"> <span style="font-size: 30px; font-weight:600; color:#FFFFFF; ">
						&nbsp; &nbsp;&nbsp; &nbsp; AKST GLOBAL</span>
				</a>
			</div>
			 <div id="main-login2" class="main-login2">
				<a href="<%=Prop.getProperty(Prop.WEBUI_LINK)%>" target="_self">
					<img name="img_webui" src="<%=Prop.getImage("img_webui2.png")%>"
					border="0" />
				</a>
				<br>
				<a target="_blank"> <span style="font-size: 20px; font-weight:600; color:#FFFFFF; ">
					&nbsp; &nbsp; AKST GLOBAL</span>
				</a>
				
			</div>
			<div id="footercopyrightsLeft">
				&copy; Copyright 2013-<%=Prop.getYear()%> Major iDempiere - All rights reserved -
				<br>
				
				<a>
				&copy; Copyright 2020-<%=Prop.getYear()%> Minor AKST Global - All rights reserved -
			</a>
			</div>
				
			
		</div>
		<div class="rectangle1">
			<div id="iBreak" class="iBreakFree-text">
				<a style="text-decoration: none" target="_blank"> <span>
						iReady</span><span style="color: rgb(137, 136, 214);">Soft</span>
				</a>
			</div>
			<div id="foot" class="foot">
				<div id="socialBar" class="socialBar">
					<%
						if (!Prop.getProperty(Prop.SOC_FACEBOOK).equals("")
								&& !Prop.getProperty(Prop.SOC_FACEBOOK).equals("NONE")) {
					%>
					<a style="text-decoration: none" id="facebook"
						href="<%=Prop.getProperty(Prop.SOC_FACEBOOK)%>" target="_blank">
						<img name="img_facebook"
						src="<%=Prop.getImage("img_facebook.png")%>" border="0" />
					</a>
					<%
						}
						if (!Prop.getProperty(Prop.SOC_TWITTER).equals("") && !Prop.getProperty(Prop.SOC_TWITTER).equals("NONE")) {
					%>
					<a style="text-decoration: none" id="twitter"
						href="">
						<img name="img_twitter"
						src="<%=Prop.getImage("img_twitter.png")%>" border="0" />
					</a>
					<%
						}
						if (!Prop.getProperty(Prop.SOC_LINKEDIN).equals("")
								&& !Prop.getProperty(Prop.SOC_LINKEDIN).equals("NONE")) {
					%>
					<a style="text-decoration: none" id="linkedin"
						href="<%=Prop.getProperty(Prop.SOC_LINKEDIN)%>" target="_blank">
						<img name="img_linkedin"
						src="<%=Prop.getImage("img_linkedin.png")%>" border="0" />
					</a>
					<%
						}
						if (!Prop.getProperty(Prop.SOC_GOOPLUS).equals("") && !Prop.getProperty(Prop.SOC_GOOPLUS).equals("NONE")) {
					%>
					<a id="google+" href="<%=Prop.getProperty(Prop.SOC_GOOPLUS)%>"
						target="_blank"> <img name="img_googleplus"
						src="<%=Prop.getImage("img_googleplus.png")%>" border="0" />
					</a>
					<%
						}
						if (!Prop.getProperty(Prop.SOC_5).equals("") && !Prop.getProperty(Prop.SOC_5).equals("NONE")) {
					%>
					<a id="soc5" href="<%=Prop.getProperty(Prop.SOC_LINKEDIN)%>"
						target="_blank"> <img name="img_soc5"
						src="<%=Prop.getImage("img_soc5.png")%>" border="0" />
					</a>
					<%
						}
						if (!Prop.getProperty(Prop.SOC_6).equals("") && !Prop.getProperty(Prop.SOC_6).equals("NONE")) {
					%>
					<a id="soc6" href="<%=Prop.getProperty(Prop.SOC_LINKEDIN)%>"
						target="_blank"> <img name="img_soc6"
						src="<%=Prop.getImage("img_soc6.png")%>" border="0" />
					</a>
					<%
						}
						if (!Prop.getProperty(Prop.SOC_7).equals("") && !Prop.getProperty(Prop.SOC_7).equals("NONE")) {
					%>
					<a id="soc7" href="<%=Prop.getProperty(Prop.SOC_LINKEDIN)%>"
						target="_blank"> <img name="img_soc7"
						src="<%=Prop.getImage("img_soc7.png")%>" border="0" />
					</a>
					<%
						}
					%>
				</div>
				<div id="idempierelink" class="idempiere_link">
					<a style="text-decoration: none"
						href="">
						<img name="img_option" src="<%=Prop.getImage("img_option.png")%>"
						border="0" title="<%=Prop.getProperty(Prop.OPTION_TEXT)%>" />
					</a> <a style="text-decoration: none" id="felixConsole"
						href="<%=Prop.getProperty(Prop.FELIX_LINK)%>"> <img
						name="img_felix" src="<%=Prop.getImage("img_felix.png")%>"
						border="0" title="<%=Prop.getProperty(Prop.FELIX_TEXT)%>" />
					</a> <a style="text-decoration: none" id="ServerManagement"
						href="<%=Prop.getProperty(Prop.MONITOR_LINK)%>"> <img
						name="img_idempiereMonitor"
						src="<%=Prop.getImage("img_idempiereMonitor.png")%>
					"
						border="0" title="<%=Prop.getProperty(Prop.MONITOR_TEXT)%>" />
					</a>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</div>
				<div id="aboutus" class="aboutus_link">
					<a style="color: black; text-decoration: none" target="_blank"
						href="<%=Prop.getProperty(Prop.SUPPORTREQ_LINK)%>"> <span>
							About Us</span>
					</a> 
						</div>
					<div id="ibreakfree" class="ibreakfree_link"> <a style="color: black; text-decoration: none"
						href="<%=Prop.getProperty(Prop.FORUMS_LINK)%>" target="_blank">
						<span> iReadySoft</span>
					</a>
					</div>
			
				<div id="footercopyright">Powered by iDempiere</div>
				<div id="footercopyrightRight">
				<a href=" " target="blank">License is GPLv2</a>
		<br>
			<a href=" " target="blank">iReadySoft License is GPLv2</a>
		
			</div>
				
			</div>
		</div>

	</section>
</body>
</html>
